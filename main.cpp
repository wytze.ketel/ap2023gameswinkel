#include <ctime>
#include <iostream>
#include <list>

#include <algorithm>
#include <math.h>

#include <iterator>
#include <stdio.h>
// -*- lsst-c++ -*-
/*
 * Autheur: Wytze A. Ketel
 * Datum: 10-5-2022
 * Opdracht(en) Maken van een Gameswinkel.
 * Hiervoor progammeren 2 klassen. 
 * 1. Game en 2.Person
 * Game klasse 
 * 1.11 -title : std::string
 * 1.12 -releaseDate : int
 * 1.13 -originalPrice : float
 * 1.21 +getTitle() : std::string
 * 1.22 +setTitl(title : std::string) : void
 * 1.23 +getReleaseDate() : int
 * 1.24 +setReleaseDate(ReleaseDate : int) : void
 * 1.25 +getOriginalPrice() : float
 * 1.26 +setOriginalPrice(originalPrice: float) : void
 * 1.27 +currentPrice() : float
 * 1.3  +Game(title:std::string, releaseDate: int, originalPrice: float)
 * 
 * Person klasse
 * 1.11 -budget : float
 * 1.12 -name : std::string
 * 1.21 +buy(game: Game) : bool
 * 1.22 +sell(game: Game, buyer : Person&):bool
 * 1.23 +getBudget() : float
 * 1.24 +setBudget(budget: float):void
 * 1.25 +getGames(: std::list<Game>):void
 * 1.26 +setGames(games:std::list<Game):void
 * 1.27 +addGame(game:Game) : void
 * 1.28 +removeGame(game: Game) :void
 * 1.29 +getName(): std::string
 * 1.2a +setName(name : std::string) :void
 * 1.3  +Person(budget : float, name : std::string)
*/

/// @brief Game object
class Game
{
    private:
        std::string title;
        int releaseDate;
        float originalPrice;
    public:
        /// Operator overloads so that remove() can figure out what is what.
        bool operator == (const Game& g) const { return title == g.title && releaseDate == g.releaseDate && originalPrice == g.originalPrice;}
        bool operator != (const Game& g) const { return !operator==(g);}
        /// @brief Initializer, creates the game object.
        /// @param text The game's title
        /// @param integer The game's release year
        /// @param floater The game's release price (currency agnostic)
        Game(std::string text, int integer, float floater)
        {
            title = text;
            releaseDate = integer;
            originalPrice = floater;
        }
        /// @brief returns the game objects title
        /// @return string format title
        std::string getTitle()
        {
            return title;
        }
        /// @brief Updates the game objects title
        /// @param newTitle String of the new title
        void setTitle(std::string newTitle)
        {
            title = newTitle;
        }
        /// @brief returns the game objects release year
        /// @return int release year
        int getReleaseDate()
        {
            return releaseDate;
        }
        /// @brief Updates the game objects release year
        /// @param newReleaseDate int release year 
        void setReleaseDate(int newReleaseDate)
        {
            releaseDate = newReleaseDate;
        }
        /// @brief returns the game objects original price.
        /// @return float original price
        float getOriginalPrice()
        {
            return originalPrice;
        }
        /// @brief updates the game objects origianl price value
        /// @param newOriginalPrice new price value
        void setOriginalPrice(float newOriginalPrice)
        {
            originalPrice = newOriginalPrice;
        }
        /// @brief calculates current price, uses game object's original release year, this assumes a 30% deprication in game value each year.
        /// @return float game's current price
        float currentPrice()
        {
            return originalPrice * pow(0.70, 2022 - releaseDate);
        }
};

/// @brief Person object 
class Person
{
    private:
        float budget;
        std::string name;
        std::list<Game> gamesList;
    public:
        /// @brief Constructor for the Person object
        /// @param text Person's name string
        /// @param floater Person's budget float
        /// @param initialGames The games initially owned by the person, if not used then assumed to be empty.
        Person(std::string text, float floater, std::list<Game> initialGames = {}) // , std::list<Game> initialGames = {}
        {
            budget = floater;
            name = text;
            gamesList = initialGames;
        }
        /// @brief Functionality to buy game initially from the void.
        /// @param game Game to be bought.
        /// @return True if transaction succeful, False if transaction unsuccesful.
        bool buy(Game game)
        {
            if (budget > game.currentPrice()){
                if(hasGame(game)){
                    return false;
                }
                else{
                    gamesList.push_back(game);
                    budget = budget - game.currentPrice();
                    return true;
                }
            }
            else{
                return false;
            }
        }
        /// @brief Functionality to buy game from other Person class entity.
        /// @param game Game to be bought.
        /// @param seller Entity selling the game.
        /// @return True if transaction succesful, False if transaction unsuccesful.
        bool buyFrom(Game game, Person seller)
        {
            /// Checking if the buyer even has the funds to buy the game.
            if (budget > game.currentPrice()){
                /// checking if the buyer already owns the game.
                if (hasGame(game) == false){
                    /// Checking if the seller owns the game.
                    if(seller.hasGame(game)){
                        gamesList.push_back(game);
                        budget = budget - game.currentPrice();
                        seller.removeGame(game);
                        seller.sell(game);
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }            
        }
        /// @brief Updates budget with the salesvalue of the game and removes the game from owned games.
        /// @param game 
        void sell(Game game)
        {
            // std::cout << getName() << " is selling! " << game.getTitle() << " for: " << game.currentPrice() << "\n";
            gamesList.remove(game);
            // std::cout << getBudget() << "\n";
            budget =  budget + game.currentPrice();
            // std::cout << getBudget() << "\n";
            // return true;
        }
        /// @brief Check to see if the game is already on gamesList.
        /// @param game The object to check for.
        /// @return True if object is in gamesList, False if not.
        bool hasGame(Game game)
        {
            if (gamesList.empty()){
                return false;
            }
            else if(std::find(gamesList.begin(), gamesList.end(), game) != gamesList.end()){
                return true;
            }
            else{
                return false;
            }
        }
        std::list<Game> getGames(){
            return gamesList;
        }
        float getBudget()
        {
            return budget;
        }
        void setBudget(float newBudget)
        {
            budget = newBudget;
        }
        void setGames(std::list<Game> newGames)
        {
            gamesList = newGames;
        }
        void addGame(Game game)
        {
            gamesList.push_back(game);
        }
        void removeGame(Game game)
        {
            gamesList.remove(game);
        }
        std::string getName()
        {
            return name;
        }
        void setName(std::string newName)
        {
            name = newName;
        }
};

std::string gamesListToString(std::list<Game> gamesList){
    std::list<Game>::iterator it;
    std::string result;
    for (it = gamesList.begin(); it != gamesList.end(); ++it){
        result += it->getTitle() + ", released " + std::to_string(it->getReleaseDate()) + "; original price: €" + std::to_string(it->getOriginalPrice()) +" now: €" + std::to_string(it->currentPrice()) +".\n";
    }
    return result;
}

//Collects information on Person person, and prints predefined information about the person.
void personenConsolePrint(Person person){
    std::string gamesText = gamesListToString(person.getGames());
    std::cout << person.getName() << "'s budget is €" << person.getBudget() << " and owns: \n" << gamesText << std::endl;
}

int main()
{
    std::cout << "Gameswinkel running. \n";
    std::time_t result = std::time(NULL);
    std::cout << std::asctime(std::localtime(&result));
    time_t curr_time = time(NULL);
    tm *tm_local = localtime(&curr_time);
    int releaseDate1 = tm_local->tm_year + 1899; // 1 jaar geleden
    int releaseDate2 = tm_local->tm_year + 1898; // 2 jaar geleden
    
    Game g1("Just Cause 3", releaseDate1 , 49.98);
    Game g2("Need for Speed: Rivals", releaseDate2 , 45.99);
    Game g3("Need for Speed: Rivals", releaseDate2 , 45.99);
    Game g4("Testy expensive: Bankrupcy edition", releaseDate2, 500000);

    std::list<Game> gamesList;
    gamesList.push_back(g1);
    gamesList.push_back(g2);
    gamesList.push_back(g3);

    Person p0("Gameshop INC", 5000, gamesList); /// Sadly this didn't quite work out, as remove sees both games as equal!.
    Person p1("Eric", 200);
    Person p2("Hans", 55);
    Person p3("Arno", 185);
    
    std::cout << std::boolalpha;
    // personenConsolePrint(p0);

    std::cout << "p1 buys g1: " << p1.buyFrom(g1,p0) << "\n";
    std::cout << "p1 buys g2: " << p1.buyFrom(g2,p0) << "\n";
    std::cout << "p1 buys g3: " << p1.buy(g3) << "\n";
    std::cout << "p2 buys g2: " << p2.buyFrom(g2,p1) << "\n";
    std::cout << "p2 buys g1: " << p2.buyFrom(g1,p1) << "\n";
    std::cout << "p3 buys g3: " << p3.buy(g3) << "\n";

    personenConsolePrint(p1);
    personenConsolePrint(p2);
    personenConsolePrint(p3);
    
    std::cout << "Gameswinkel ran.";
    return 0;
}